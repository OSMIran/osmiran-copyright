import fetch from "node-fetch";
import ejs from "ejs";
import { rm, mkdir, writeFile } from "fs/promises";
import { copy } from "fs-extra";
import { join } from "path";
import { buildFolder, indexFile, rootFolder } from "./paths.js";

const parseOkFail = (x) => {
  if (x.search('رعایت') !== -1) return 'fail';
  if (x.search('کامل') !== -1) return 'ok';
  if (x.search('ناقض') !== -1) return 'partial';
  if (x.search('اشتقاقی') !== -1) return 'non derivative';
  return 'no info';
};

const parseTable = (html) => {
  const htmp = html.slice(html.search('</table>')+1);
  const start = htmp.search('<table');
  const end = htmp.search('</table>');
  const table = htmp.slice(start, end).split('<tr>');
  return table.map((tr) => {
    const tds = tr.split('</td>');
    let hrefps = tds[0].search('href');
    if (hrefps === -1) return { name: "" };
    const a = tds[0].slice(hrefps+6);
    const name = a.slice(a.search('>')+1, a.search('</a>'));
    const link = a.slice(0, a.search('"'));
    const attrib = parseOkFail(tds[1]);
    const sharealike = parseOkFail(tds[2]);
    const date = tds[5].replace('<td>', '').trim();
    return { name, link, attrib, sharealike, date };
  }).filter((x) => x.name !== '').sort((a, b) => a.name.localeCompare(b.name, 'fa'));
};

const main = async () => {
  const res = await fetch("https://wiki.openstreetmap.org/wiki/Fa:Lacking_proper_attribution");
  const data = parseTable(await res.text());
  const html = await ejs.renderFile(indexFile, {
    data,
    date: (new Date).toLocaleDateString('fa-ir', {
      year: 'numeric', month: 'long', day: 'numeric',
    }),
  });
  await rm(buildFolder, { recursive: true, force: true });
  await mkdir(buildFolder);
  await copy(join(rootFolder, 'dist'), join(buildFolder, 'dist'));
  await writeFile(join(buildFolder, 'index.html'), html);
};

main();
